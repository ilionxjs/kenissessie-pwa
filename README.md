# PWA
## Installatie
* Lighthouse browser extension

## Eerste analyse
* Start de applicatie met `npm start`
* Open de url `localhost:4200` in de browser
* Open de Chrome Developer Tools, ga naar tabblad `Audit` en voer een `Lighthouse` scan uit.

## Manifest
* Voeg `manifest.json` toe aan `/src`. Het bestand staat onder `resources/manifest`;
* Link naar de `manifest` vanuit `index.html` met `<link>`
*	Voeg in `.angular-cli` onder `assets` het `manifest.json` toe, zodat het bestand meegepacked wordt
* start de applicatie

In Chrome kan je in de Developer tools zien of het is gelukt. Ga hiervoor naar `Application`, waar je `Manifest` ziet staan.

* speel met de settings in `manifest.json` om bijvoorbeeld een andere kleur te krijgen.

## ServiceWorker
* installeer  "@angular/service-worker": "^5.2.9"
* Voeg in `.angular-cli` onder `apps.0` de property `serviceWorker` toe, en zet deze op true
* Importeer de `ServiceWorkerModule` in `AppModule`, en configureer deze voor productie:
```javascript
.register('/ngsw-worker.js', { enabled: environment.production })
```
* Kopieer `resources/serviceworker/ngsw-config.json` naar de `src` folder
* Bouw de applicatie met `ng build --prod`
* Start de applicatie met het commando `npx http-server -c-1 dist/`

Kijk weer in de Chrome Developer Tools onder het tabblad `Application`. Onder `serviceworker` kan je de geregistreerde serviceworker zien.


* Bij `Manifest` kan je nu de applicatie aan je homescreen toevoegen.

### nieuwe app deploy
Om de app up te daten biedt Angular een `SwUpdate` service aan.

* Injecteer deze in `AppComponent`, en in `ngOnInit` zet onderstaande code:
```javascript
if (this.swUpdate.isEnabled) {
    this.swUpdate.available.subscribe(() => {
        if(confirm("New version available. Load New Version?")) {
            window.location.reload();
        }
    });
}       
```
* Maak nu een nieuwe build, met een wijziging zodat er nieuwe resources worden gegenereerd.
* controleer de `ngsw-config.json` of de nieuwe resources erin staat
* Open het netwerk tabblad van de Developer Tools voordat je de pagina ververst.
* Refresh de pagina

Je ziet nu onder de `Size` kolom dat de oude resources van de bestanden van de ServiceWorker komen. 
Na een paar seconden komt de call naar `ngsw-config.json` erbij, wat controleert of er nieuwere bestanden zijn.
Als er nieuwe bestanden zijn, wordt de `SwUpdate` service aangeroepen en krijgt de gebruiker een prompt om te refreshen.


### resources
* https://angular.io/guide/service-worker-intro
* https://blog.angular-university.io/angular-service-worker/

## Prerender
* install dev dependencies
`npm install @nguniversal/express-engine @nguniversal/module-map-ngfactory-loader fs@0.0.1-security -D`

```
"@nguniversal/express-engine": "^5.0.0-beta.5",
"@nguniversal/module-map-ngfactory-loader": "^5.0.0-beta.5",
"fs": "0.0.1-security"
```

* Voeg onderstaande scripts toe aan `package.json`
```
"build:render": "ng build --target=production && ng build --target=production --app prerender --output-hashing=none",
"postbuild:render": "npm run render",
"render": "ts-node prerender.ts",
"render:debug": "TS_NODE_DEBUG=true ts-node prerender.ts"
```
* In `.angular-cli`, moeten we een nieuwe app registreren die code genereerd naast onze `app`. Voeg hiervoor onderstaande configuratie toe aan `apps: []`
```
{
  "name": "prerender",
  "platform": "server",
  "root": "src",
  "outDir": "dist-prerender",
  "main": "main.prerender.ts",
  "tsconfig": "tsconfig.prerender.json",
  "environmentSource": "environments/environment.ts",
  "environments": {
    "dev": "environments/environment.ts",
    "prod": "environments/environment.prod.ts"
  }
}
```
Dit bestand wordt gebruikt door de CLI om een aparte deployable te genereren, die we gebruiken om de view te renderen tijdens de build.
Hiervoor zijn nog een paar configuratie bestanden nodig:
1. prerender.ts
2. tsconfig.prerender.ts
3. main.prerender.ts
4. app.module.prerender.ts

* `prerender.ts` staat klaar onder `resourses/prerender/`. Deze kan je kopieren naar de root folder `pwa`
* Kopieer de bestanden `tsconfig.prerender.json` en `main.prerender.ts` naar de `src/` folder.
* Kopieer `app.module.prerender.ts` naar `src/app`.
* Pas de import van `BrowserModule` in `app.module.ts` aan naar onderstaande, Voor `appId` mag je ook zelf iets verzinnen.
```javascript
BrowserModule.withServerTransition({ appId: 'pwa' }),
```

* Draai het script `build` met `npm run build`. Let op: deze duurt nu iets langer.
* Als je een foutmelding krijgt: `ReferenceError: System is not defined`, dat is voor nu niet erg. De prerendering is wel uitgevoerd

Wanneer je nu de `index.html` opent in de `dist` folder, zal je meer zien dan alleen `ipv-root`.

* Doe weer een lighthouse audit om het verschil in score te zien.

### analyse
* Kijk in het bestand `prerender.ts`, hier wordt de rendering van de views echt gedaan. 
* Probeer ook alle toegevoegde bestanden terug te vinden in de aangemaakt configuratie.

### resources
* [https://blog.strongbrew.io/prerendering-angular-apps](Prerender Blog)
* [https://github.com/strongbrewio/prerender-angular-example](Prerender Example) (kijk naar de branches)

# server side rendering
* installeer dev dependencies
`npm i express@4.16.3 @types/express@4.11.1 webpack-cli@^2.0.10 @nguniversal/express-engine @nguniversal/module-map-ngfactory-loader ts-loader@^3.5.0 -D`

```
"express": "4.16.3",
"@types/express": "4.11.1",
"webpack-cli": "^2.0.10",
"@nguniversal/express-engine": "^5.0.0-beta.8",
"@nguniversal/module-map-ngfactory-loader": "^5.0.0-beta.8",
```

* scripts uitbreiden
```
"webpack": "webpack",
"build:universal": "npm run build:client-and-server-bundles && npm run webpack:server",
"serve:universal": "node dist/server.js",
"build:client-and-server-bundles": "ng build --prod && ng build --prod --app 1 --output-hashing=false",
"webpack:server": "webpack --config webpack.server.config.js --progress --colors"
```

* In `.angular-cli`, moeten we een nieuwe app registreren die code genereerd naast onze `app`. Voeg hiervoor onderstaande configuratie toe aan `apps: []`
```javascript
{
  "platform": "server",
  "root": "src",
  "outDir": "dist/server",
  "assets": [
    "assets",
    "favicon.ico"
  ],
  "index": "index.html",
  "main": "main.server.ts",
  "test": "test.ts",
  "tsconfig": "tsconfig.server.json",
  "prefix": "ipv",
  "styles": [
    "styles.css"
  ],
  "scripts": [],
  "environmentSource": "environments/environment.ts",
  "environments": {
    "dev": "environments/environment.ts",
    "prod": "environments/environment.prod.ts"
  }
}
```
* pas de `outDir` property van de eerste app aan naar `dist/browser`.

Verder hebben we nog een paar configuratie bestanden nodig
1. server.ts
2. webpack.server.config.js
3. tsconfig.server.ts
4. main.server.ts
5. app.module.server.ts

* `server.ts` & `webpack.server.config.js` staan klaar onder `resourses/ssr/`. Deze kan je kopieren naar de root folder `pwa`
* Kopieer de bestanden `tsconfig.server.json` en `main.server.ts` naar de `src/` folder.
* Kopieer `app.module.server.ts` naar `src/app`.
* Pas de import van `BrowserModule` in `app.module.ts` aan naar onderstaande, Voor `appId` mag je ook zelf iets verzinnen.
```javascript
BrowserModule.withServerTransition({ appId: 'pwa' }),
```

* Draai de SSR build  met het commando `npm run build:universal`. 
* Start de server met `npm run serve:universal`. Ga naar het adres `localhost:4000` voor het resultaat.

* Draai de Lighthouse audit weer voor de nieuwe score.

### analyse
* Kijk in het bestand `server.ts`, hier wordt de runtime rendering van de views echt gedaan. 
* Probeer ook alle toegevoegde bestanden terug te vinden in de aangemaakt configuratie.

### resources
* [https://angular.io/guide/universal](Angular Universal)
* [https://github.com/angular/angular-cli/blob/master/docs/documentation/stories/universal-rendering.md](Angular ClI Universal)
* [Azure functions](https://azure.microsoft.com/en-us/services/functions/)
* [https://firebase.google.com/docs/functions](Firebase functions)
* [https://github.com/GoogleChrome/rendertron](Rendertron)

## Noscript
Voeg nog een `<noscript>` tag toe om de score nog dat laatste beetje omhoog te tillen.
