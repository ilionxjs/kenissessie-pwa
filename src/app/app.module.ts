import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LoginComponent} from './login/login.component';
import {PreloadAllModules, RouterModule} from '@angular/router';
import {CoreModule} from "./core/core.module";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([
      {
        path: '',
        component: LoginComponent
      },
      {
        path: 'projects',
        loadChildren:
          'app/project-voter/project-voter.module#ProjectVoterModule'
      }
    ], {
      preloadingStrategy: PreloadAllModules
    }),
    CoreModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
