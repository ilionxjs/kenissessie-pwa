import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule, MatCardModule, MatInputModule} from "@angular/material";

@NgModule({
  imports: [CommonModule],
  exports: [
    ReactiveFormsModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
  ],
  declarations: []
})
export class CoreModule {}
