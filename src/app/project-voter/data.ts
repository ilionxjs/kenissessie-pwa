export const data = [
  {
    description:
      'IntelliJ/WebStorm plugin voor het beter kunnen werken aan Angular componenten door gerelateerde bestanden in 1 view te openen\n',
    goal:
      'Efficient kunnen werken aan Angular applicaties, uiteindelijk te releasen via jetbrains repository.',
    key: '-L75-iYxPaGVQQd_aZbu',
    name: 'Angular Component View',
    tasks: [
      { name: 'Default settings scherm' },
      { name: 'Betere manier van bestanden tonen' },
      { name: 'Layout verbeteringen' }
    ],
    user: 'gboer@ilionx.com',
    votes: ['riwema@ilionx.com', 'mvollebregt@ilionx.com', 'gboer@ilionx.com']
  },
  {
    description:
      "Developers houden niet van documentatie lezen, laat staan schrijven. Toch is het wel handig om documentatie te hebben en te onderhouden, het liefst zo dicht mogelijk tegen de code aan. Met Swagger bv kun je je REST API definiëren in de code dmv annotatie's. Hetzelfde zou je kunnen doen met annotatie's voor het vastleggen van de applicatie flow van controller naar backend services.\n\nMet tools zoals PlantUml kun je diverse diagram types laten genereren (png, gif etc), er zijn ook Confluence plugins die plantUml direct kunnen weergeven. Je kunt plantUml ook rechtstreeks editen en viewen in Intellij idea dmv een plugin.\n\n",
    goal: 'genereren van documentatie',
    key: '-L7AVKwqswgc1I7GDh9A',
    name: 'Genereren van Sequence diagram aan de hand van annotaties',
    tasks: [
      { name: "defineren van annotatie's in een Java jar" },
      {
        name: 'genereren van een plantUml text bestand dmv een Maven plugin'
      }
    ],
    user: 'avanharen@ilionx.com',
    votes: ['mvollebregt@ilionx.com', 'midejong@ilionx.com']
  },
  {
    description:
      'Als je een integratietest wilt maken voor een applicatie die rabbitmq gebruikt bestaat er geen kant en klare embedded amqp server, zoals je bijvoorbeeld voor databases h2 kan gebruiken. Om je testen te doen kun je of rabbitmq starten of je kan met Apache qpid (https://qpid.apache.org/) een embedded amqp server configureren.\nVoor spring boot heb je cloud wiremock (https://cloud.spring.io/spring-cloud-contract/multi/multi__spring_cloud_contract_wiremock.html), wat er voor zorgt dat je met een annotatie en een dependency, wiremock in je test beschikbaar hebt.\nHet zou mooi zijn om een zelfde soort library te maken alleen dan voor een embedded amqp server.\n',
    goal:
      'Eenvoudig een embedded amqp server beschikbaar maken voor in een test',
    key: '-L7L3Xld301m3mhSujKL',
    name: 'Embedded amqp server voor test',
    tasks: [
      {
        name:
          'Maken van een library die dmv een annotatie een embedded amqpserver beschikbaar maakt in een test en deze start en stopt '
      },
      {
        name:
          'De embedded server starten op een random vrije poort en deze poort beschikbaar maken in de test voor de configuratie'
      }
    ],
    user: 'evanbeest@ilionx.com',
    votes: ['riwema@ilionx.com', 'evanbeest@ilionx.com']
  },
  {
    description:
      "Binnen ilionx hebben we aardig wat ervaring met Angular, maar nog niet iedereen heeft even veel ervaring met React.\n\nWe maken een kleine applicatie in React waarin je kunt opzoeken welke collega's bij welke opdrachtgevers zitten.",
    goal: 'React',
    key: '-L7dZ3S9mddbeHL5o05U',
    name: 'Iets met React',
    tasks: [
      {
        name:
          "Scherm maken met overzicht van alle collega's met alle opdrachtgevers"
      },
      { name: 'Scherm koppelen aan een backend' },
      { name: 'Kleine backend schrijven' },
      { name: 'Scherm maken om informatie aan te passen' }
    ],
    user: 'mvollebregt@ilionx.com',
    votes: [
      'nberlijn@ilionx.com',
      'riwema@ilionx.com',
      'mvollebregt@ilionx.com'
    ]
  },
  {
    description:
      'Volgens mij is dit een project wat al een tijdje klaar staat om uit te voeren, echter in het verleden is hier geen tijd voor geweest. Misschien nu een mooie gelegenheid om dit op te pakken. Ronald Bouwmeister heeft alle spullen in huis en met een dagje knutselen moet het mogelijk zijn om een live scoreboard te maken voor het tafelvoetbalspel. Hier kunnen we natuurlijk mooi gebruik van maken in Zwolle, maar het zal ook een extra trigger zijn voor als de tafelvoetbal op een beurs staat.',
    goal:
      'Het omzetten van activiteiten (in dit geval een bal door een sensor) naar een visuele weergave (het scoreboard)',
    key: '-L7tlGdHnmAyFeNHrbJn',
    name: 'Tafelvoetbal Live Score',
    tasks: [
      {
        name: 'Sensoren maken voor het registreren van de doelpunten'
      },
      {
        name: 'Koppelen van deze sensoren aan een base module (raspberry pi)'
      },
      { name: 'Opslaan van data in een database' },
      { name: 'Weergave van de score op een scoreboard' },
      {
        name:
          'Reset knoppen voor het verwijderen van foutieve doelpunten of een reset voor een nieuw spel'
      }
    ],
    user: 'pboldingh@ilionx.com',
    votes: [
      'rbouwmeister@ilionx.com',
      'riwema@ilionx.com',
      'akleis@ilionx.com',
      'jheidstra@ilionx.com',
      'rbrouwer@ilionx.com',
      'pboldingh@ilionx.com'
    ]
  },
  {
    description:
      "We gaan binnenkort enkele applicaties hosten (Petplan, Aboma, test omgeving Gebroeders Janssen) waarvan wij ook het beheer gaan doen. Het zou mooi zijn als we een scherm kunnen maken die we overdag aan kunnen zetten waarin de status van de diverse omgevingen worden weergegeven. We zijn begonnen met een Spring Boot admin applicatie waarin we kunnen zien of één van de Spring Boot applicaties eruit ligt. Het zou mooi zijn om ook te kunnen registreren of de Front End nog up and running is, de gateway nog goed draait en of de achterliggende databases nog reageren. Er zal vast monitoring beschikbaar zijn, maar die beslaan meestal niet alles wat je wil. Als we eenmaal een basis omgeving hebben dan kunnen we dit uitbreiden met controle op logging, notificatie als een omgeving plat gaat of tegen het einde van z'n resources aanloopt, ect. Helemaal mooi zou zijn als we dit zo kunnen inrichten dat alles op één scherm aanwezig is.",
    goal:
      'Communicatie met de onderliggende systemen, dat vertalen in een Dashboard waarin enkel de meest belangrijke zaken worden weergegeven.',
    key: '-L7tmgxGUJqc9sHyHz1y',
    name: 'Applicatie monitoring',
    tasks: [
      {
        name: 'Uitzoeken wat er beschikbaar is aan monitoring tools'
      }
    ],
    user: 'pboldingh@ilionx.com',
    votes: [
      'nberlijn@ilionx.com',
      'evanbeest@ilionx.com',
      'riwema@ilionx.com',
      'pboldingh@ilionx.com'
    ]
  },
  {
    description:
      'Volgens mij mochten we ook prive projecten hier kwijt. Ik ben momenteel bezig met een monitoring scherm voor mij roei apparaat. Deze heb ik al een tijdje, maar ik vind de computer die eraan hangt wat te beperkt dus ben zelf aan de knutsel gegaan. Ik heb inmiddels een raspberry pi gekoppeld aan de roeier die de data pakketjes opvangt die worden verstuurd. Ik heb daarnaast een Polar hartslag meter die gekoppeld is aan een app. Deze hartslag meter wil ik graag koppelen aan de applicatie die ik heb gemaakt voor het weergegeven van de roei data. Hiervoor is het nodig dat de Pi wordt gekoppeld met de hartslag meter, deze wordt uitgelezen. De data moet worden opgeslagen en moet daarbij ook getoond worden op het dashboard.\n\nHet dashboard staat hier:\nhttp://www.prisco.nl:8088/ en als je dan voor demo kiest dan zie je een sessie die ik een tijdje geleden heb gedaan. Leuk die getallen, maar wil daar graag nog de hartslag aan toevoegen.',
    goal:
      'Koppelingen van externe apparatuur aan een Pi, opvangen van communicatie en het vertalen van de data naar bruikbare en leesbare gegevens.',
    key: '-L7toEsrfnJnr_eOgfnB',
    name: 'Koppeling Hartslagmeter met Raspberry Pi 3',
    tasks: [
      {
        name: 'Uitzoeken hoe de hartslag meter kan worden gekoppeld'
      },
      { name: 'Opvangen, uitlezen en opslaan van de data' },
      { name: 'Weergave van deze dat op het dashboard' }
    ],
    user: 'pboldingh@ilionx.com',
    votes: [
      'nberlijn@ilionx.com',
      'jheidstra@ilionx.com',
      'pboldingh@ilionx.com'
    ]
  },
  {
    description:
      'Een project waar waarschijnlijk meer tijd gaat zitten in het bedenken van de manier van registreren dan in het uitvoeren. We hebben te maken met Flex plekken in Utrecht en in Zwolle. Er zijn dagen (die zijn er altijd in Utrecht) dat het druk is en het zou fijn zijn om dat te kunnen zien voordat je vertrekt. De vraag is hoe registreer je dat? Dat kan via het uitlezen van het Wifi punt, kan zijn door te registeren hoeveel personen door de deur lopen, maar dan is de uitdaging hoe en welke kant gaat deze persoon op. Je zou de plaats van een mobiel kunnen registeren en zo zijn er vast nog wel meer manieren om te zien hoe druk het is. Als we dat kunnen weergeven op een pagina, kan je vooraf bepalen naar welke locatie je toe gaat zodat je zeker weet dat je een plek hebt.',
    goal:
      'Hoe kan je dit bepalen en hoe kan je deze data opvangen en vertalen naar één uitkomst, in dit geval niet druk / beetje druk / heel erg druk, etc',
    key: '-L7tpiBm25W8NU5R8-1O',
    name: 'Hoe druk is het?',
    tasks: [
      {
        name: 'Een manier bendenken om de drukte te kunnen bepalen'
      },
      {
        name:
          'Als deze manier is bepaald een manier gaan vinden om deze data te ontvangen'
      },
      {
        name:
          'Deze gegevens op een pagina weergeven die kan worden geraadpleegd met een advies'
      }
    ],
    user: 'pboldingh@ilionx.com',
    votes: ['nberlijn@ilionx.com', 'pboldingh@ilionx.com']
  }
];
