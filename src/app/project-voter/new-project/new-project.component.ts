import {
  Component,
  EventEmitter,
  OnInit,
  Output
} from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';

@Component({
  selector: 'ipv-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.css']
})
export class NewProjectComponent implements OnInit {
  @Output() add = new EventEmitter<any>();

  newProject: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    const tasks = [this.newProjectTaskGroup()];
    this.newProject = this.fb.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      goal: ['', [Validators.required]],
      tasks: this.fb.array(tasks)
    });
  }

  addTask() {
    if (this.projectTasks.length < 5) {
      const newTask = this.newProjectTaskGroup();
      this.projectTasks.push(newTask);
    }
  }

  removeTask(index: number) {
    this.projectTasks.removeAt(index);
  }

  submitNewProject() {
    if (this.newProject.valid) {
      const newProject = {
        ...this.newProject.value,
        votes: []
      };
      this.add.emit(newProject);
    }
  }

  get name() {
    return this.newProject.get('name') as FormControl;
  }

  get description() {
    return this.newProject.get('description') as FormControl;
  }

  get goal() {
    return this.newProject.get('goal') as FormControl;
  }

  get projectTasks() {
    return this.newProject.get('tasks') as FormArray;
  }

  private newProjectTaskGroup(): FormGroup {
    return this.fb.group({
      name: ['', [Validators.required]]
    });
  }
}
