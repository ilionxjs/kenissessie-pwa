import {Component, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {map} from 'rxjs/operators';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {ProjectService} from "../project.service";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'ipv-project-container',
  templateUrl: './project-container.component.html',
  styleUrls: ['./project-container.component.css'],
  animations: [
    trigger('flyInOut', [
      state('in', style({ opacity: 1, transform: 'translateX(0)' })),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-100%)'
        }),
        animate('0.3s ease-in')
      ]),
      transition('* => void', [
        animate(
          '0.3s 0.1s ease-out',
          style({
            opacity: 0,
            transform: 'translateX(100%)'
          })
        )
      ])
    ])
  ]
})
export class ProjectContainerComponent implements OnInit {
  projects$: Observable<any[]>;

  showNewProject: boolean;
  showProjects: boolean;

  user: string;


  static addKeyToProject(changes) {
    return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
  }

  static addMissingData(changes) {
    return changes.map(c => {
      if (!c.votes) {
        c.votes = [];
      }

      if (!c.suggestedTasks) {
        c.suggestedTasks = [];
      }
      return c;
    });
  }

  static sort(projects: any[]) {
    return projects.sort(
      (a, b) =>
        a.votes.length > b.votes.length
          ? -1
          : a.votes.length < b.votes.length ? 1 : 0
    );
  }

  projectId(project) {
    return project.key;
  }

  constructor(private projectService: ProjectService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(
      (paramMap: ParamMap) => (this.user = paramMap.get('email'))
    );

    this.projects$ = this.projectService.projects$
      .pipe(
        // map(ProjectContainerComponent.addKeyToProject),
        map(ProjectContainerComponent.addMissingData),
        map(ProjectContainerComponent.sort)
      );

    setTimeout(() => this.projectService.get());

    this.showProjects = true;
  }

  toggleNewProject() {
    this.showNewProject = !this.showNewProject;
  }

  toggleShowProject() {
    this.showProjects = !this.showNewProject;
  }

  addProject(newProject) {
    if (this.user) {
      this.projectService.push({ user: this.user, ...newProject });
      this.showNewProject = false;
    }
  }

  vote({ project, vote }) {
    if (this.user) {
      const votes = Array.from(new Set([this.user, ...project.votes]));
      if (!vote) {
        votes.splice(votes.indexOf(this.user), 1);
      }

      const votedProject = {
        ...project,
        votes: votes
      };

      this.updateProject(votedProject);
    }
  }

  delete(project) {
    this.projectService.remove(project.key);
  }

  newSuggestedTask({ project, suggestedTasks }) {
    const suggestedProject = {
      ...project,
      suggestedTasks: suggestedTasks
    };
    this.updateProject(suggestedProject);
  }

  private updateProject(project) {
    this.projectService.update(project.key, project);
  }
}
