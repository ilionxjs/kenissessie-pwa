import {NgModule} from '@angular/core';
import {ProjectContainerComponent} from './project-container/project-container.component';
import {RouterModule} from '@angular/router';
import {SharedModule} from "../shared/shared.module";
import {NewProjectComponent} from "./new-project/new-project.component";
import {ProjectComponent} from "./project/project.component";
import {SuggestTakComponent} from './project/suggest-task/suggest-task.component';
import {ProjectService} from "./project.service";

@NgModule({
  imports: [
    RouterModule.forChild([{ path: '', component: ProjectContainerComponent }]),
    SharedModule
  ],
  declarations: [ProjectContainerComponent, NewProjectComponent, ProjectComponent, SuggestTakComponent],
  providers: [ProjectService]
})
export class ProjectVoterModule {}
