import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { data } from './data';

@Injectable()
export class ProjectService {
  projectsRef: Subject<any> = new Subject<any>();
  projects$ = this.projectsRef.asObservable();

  private data = data;

  constructor() {}

  get() {
    this.projectsRef.next(this.data);
  }

  push(project) {
    this.data.push(project);
    this.projectsRef.next(this.data);
  }

  remove(project) {
    const index = this.data.indexOf(project);
    this.data.splice(index, 1);
    this.projectsRef.next(this.data);
  }

  update(key, project) {
    console.log(key, project);
  }
}
