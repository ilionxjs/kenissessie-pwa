import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'ipv-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {
  @Input() project: any;
  @Input() user: any;

  @Output() vote = new EventEmitter();
  @Output() delete = new EventEmitter();
  @Output() newSuggestedTask = new EventEmitter();

  voted: boolean;

  constructor() {}

  ngOnInit() {
    this.voted = this.project.votes.indexOf(this.user) > -1;
  }

  doVote() {
    this.vote.emit({ project: this.project, vote: true });
  }

  unvote() {
    this.vote.emit({ project: this.project, vote: false });
  }

  deleteProject() {
    this.delete.emit(this.project);
  }

  newTask(suggestedTasks) {
    this.newSuggestedTask.emit({
      project: this.project, suggestedTasks: suggestedTasks
    });
  }
}
