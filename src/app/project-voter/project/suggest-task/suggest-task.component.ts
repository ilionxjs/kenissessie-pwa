import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";


@Component({
  selector: 'ipv-suggest-task',
  templateUrl: './suggest-task.component.html',
  styleUrls: ['./suggest-task.component.css']
})
export class SuggestTakComponent implements OnInit {

  @Output()
  newTask = new EventEmitter();

  @Input() user: string;

  suggestTaskForm: FormGroup;

  @Input()
  suggestedTasks: any[];

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.suggestTaskForm = this.fb.group({
      name: ['', [Validators.required]]
    })
  }

  submitTask(suggestTasks) {
    this.newTask.emit(suggestTasks);
  }

  addTask() {
    if (!this.userHasTaskSubmitted() && this.taskName.valid) {
      const newTask = {
        task: this.suggestTaskForm.value.name,
        user: this.user
      };
      this.suggestedTasks.push(newTask);
      this.submitTask(this.suggestedTasks);
      this.suggestTaskForm.reset();
    }
  }

  removeTask(task: string) {
    this.suggestedTasks.splice(this.suggestedTasks.indexOf(task), 1);

    this.suggestedTasks = [...this.suggestedTasks];

    this.submitTask(this.suggestedTasks);
  }

  get taskName(): FormControl {
    return this.suggestTaskForm.get('name') as FormControl
  }

  userHasTaskSubmitted() {
    return this.suggestedTasks.filter(
      (task) => task.user === this.user
    ).length !== 0;
  }
}
